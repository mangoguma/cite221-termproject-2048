// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Tue Jun  1 20:47:35 2021
// Host        : LAPTOP-TPQ6GPPN running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/82108/Documents/vivado/term01/term01.gen/sources_1/bd/design_1/ip/design_1_top_0_0/design_1_top_0_0_stub.v
// Design      : design_1_top_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "top,Vivado 2020.2" *)
module design_1_top_0_0(clk, rstn, sw, btn, packet)
/* synthesis syn_black_box black_box_pad_pin="clk,rstn,sw[1:0],btn[3:0],packet[63:0]" */;
  input clk;
  input rstn;
  input [1:0]sw;
  input [3:0]btn;
  output [63:0]packet;
endmodule

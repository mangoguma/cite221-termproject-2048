# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "GAMEEND" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GAMEMAKE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GAMEMERGE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GAMEMOVE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GAMESTART" -parent ${Page_0}
  ipgui::add_param $IPINST -name "IDLE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MENU" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RANKREGI" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RANKVIEW" -parent ${Page_0}


}

proc update_PARAM_VALUE.GAMEEND { PARAM_VALUE.GAMEEND } {
	# Procedure called to update GAMEEND when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GAMEEND { PARAM_VALUE.GAMEEND } {
	# Procedure called to validate GAMEEND
	return true
}

proc update_PARAM_VALUE.GAMEMAKE { PARAM_VALUE.GAMEMAKE } {
	# Procedure called to update GAMEMAKE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GAMEMAKE { PARAM_VALUE.GAMEMAKE } {
	# Procedure called to validate GAMEMAKE
	return true
}

proc update_PARAM_VALUE.GAMEMERGE { PARAM_VALUE.GAMEMERGE } {
	# Procedure called to update GAMEMERGE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GAMEMERGE { PARAM_VALUE.GAMEMERGE } {
	# Procedure called to validate GAMEMERGE
	return true
}

proc update_PARAM_VALUE.GAMEMOVE { PARAM_VALUE.GAMEMOVE } {
	# Procedure called to update GAMEMOVE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GAMEMOVE { PARAM_VALUE.GAMEMOVE } {
	# Procedure called to validate GAMEMOVE
	return true
}

proc update_PARAM_VALUE.GAMESTART { PARAM_VALUE.GAMESTART } {
	# Procedure called to update GAMESTART when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GAMESTART { PARAM_VALUE.GAMESTART } {
	# Procedure called to validate GAMESTART
	return true
}

proc update_PARAM_VALUE.IDLE { PARAM_VALUE.IDLE } {
	# Procedure called to update IDLE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IDLE { PARAM_VALUE.IDLE } {
	# Procedure called to validate IDLE
	return true
}

proc update_PARAM_VALUE.MENU { PARAM_VALUE.MENU } {
	# Procedure called to update MENU when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MENU { PARAM_VALUE.MENU } {
	# Procedure called to validate MENU
	return true
}

proc update_PARAM_VALUE.RANKREGI { PARAM_VALUE.RANKREGI } {
	# Procedure called to update RANKREGI when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RANKREGI { PARAM_VALUE.RANKREGI } {
	# Procedure called to validate RANKREGI
	return true
}

proc update_PARAM_VALUE.RANKVIEW { PARAM_VALUE.RANKVIEW } {
	# Procedure called to update RANKVIEW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RANKVIEW { PARAM_VALUE.RANKVIEW } {
	# Procedure called to validate RANKVIEW
	return true
}


proc update_MODELPARAM_VALUE.IDLE { MODELPARAM_VALUE.IDLE PARAM_VALUE.IDLE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IDLE}] ${MODELPARAM_VALUE.IDLE}
}

proc update_MODELPARAM_VALUE.MENU { MODELPARAM_VALUE.MENU PARAM_VALUE.MENU } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MENU}] ${MODELPARAM_VALUE.MENU}
}

proc update_MODELPARAM_VALUE.RANKVIEW { MODELPARAM_VALUE.RANKVIEW PARAM_VALUE.RANKVIEW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RANKVIEW}] ${MODELPARAM_VALUE.RANKVIEW}
}

proc update_MODELPARAM_VALUE.RANKREGI { MODELPARAM_VALUE.RANKREGI PARAM_VALUE.RANKREGI } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RANKREGI}] ${MODELPARAM_VALUE.RANKREGI}
}

proc update_MODELPARAM_VALUE.GAMESTART { MODELPARAM_VALUE.GAMESTART PARAM_VALUE.GAMESTART } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GAMESTART}] ${MODELPARAM_VALUE.GAMESTART}
}

proc update_MODELPARAM_VALUE.GAMEMAKE { MODELPARAM_VALUE.GAMEMAKE PARAM_VALUE.GAMEMAKE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GAMEMAKE}] ${MODELPARAM_VALUE.GAMEMAKE}
}

proc update_MODELPARAM_VALUE.GAMEEND { MODELPARAM_VALUE.GAMEEND PARAM_VALUE.GAMEEND } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GAMEEND}] ${MODELPARAM_VALUE.GAMEEND}
}

proc update_MODELPARAM_VALUE.GAMEMOVE { MODELPARAM_VALUE.GAMEMOVE PARAM_VALUE.GAMEMOVE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GAMEMOVE}] ${MODELPARAM_VALUE.GAMEMOVE}
}

proc update_MODELPARAM_VALUE.GAMEMERGE { MODELPARAM_VALUE.GAMEMERGE PARAM_VALUE.GAMEMERGE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GAMEMERGE}] ${MODELPARAM_VALUE.GAMEMERGE}
}


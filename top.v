`timescale 1ns / 1ps

module top(
        input wire clk,
        input wire rstn,
        input wire [1:0] sw,
        input wire [3:0] btn,
        output reg [63:0] packet        
    );
    reg [2:0] scoreNo = 1, board[15:0];
    reg [3:0] state, next_state, Btns, obst;
    reg [15:0] score, splitScore[3:0], scoreBoard[4:0];
    reg [14:0] name[4:0];
    reg [4:0] alphabet[2:0];
    reg [1:0] finishCheck = 0;
    
    wire [15:0] rnd;
    wire start, LeftBtn, DownBtn, UpBtn, RightBtn;
    assign start = sw[1];
    assign LeftBtn = btn[3];
    assign DownBtn = btn[2];
    assign UpBtn = btn[1];
    assign RightBtn = btn[0];
    
    //parameter for state
    parameter IDLE = 0, MENU = 1, RANKVIEW = 2, RANKREGI = 3;
    parameter GAMESTART = 4, GAMEMAKE = 5, GAMEEND = 8, GAMEMOVE = 6, GAMEMERGE = 7;
    integer i, j, where, board_init=0, alpha = 0,
        obst_cnt = 0, changed = 0, obst_succ = 0, successed = 0;
    random rand (.clk(clk), .rstn(rstn), .rnd(rnd));
    debounce_better_version LEFT (.pb_1(LeftBtn), .clk(clk), .pb_out(leftBtn));
    debounce_better_version RIGHT (.pb_1(RightBtn), .clk(clk), .pb_out(rightBtn));
    debounce_better_version UP (.pb_1(UpBtn), .clk(clk), .pb_out(upBtn));
    debounce_better_version DOWN (.pb_1(DownBtn), .clk(clk), .pb_out(downBtn));
    
    always @(negedge rstn or posedge clk) begin
        if (~rstn) begin
            packet <= 0;
            state <= 0;
            Btns <= 0;
            score <= 0;
            finishCheck <= 0;
            obst <= 0;
            for(i=0;i<16;i=i+1) board[i]<=0;
            for(i=0;i<4;i=i+1) splitScore[i]<=0;
            for(i=0;i<5;i=i+1) scoreBoard[i]<=0;
            for(i=0;i<5;i=i+1) name[i]<=0;
            for(i=0;i<3;i=i+1) alphabet[i]<=0;
        end
        else begin
            state <= next_state;
            case(state)
                IDLE: begin packet <= {state, 54'd0, 2'd0, leftBtn, downBtn, upBtn, rightBtn}; end
                MENU: begin
                    obst <= 0;
                    scoreNo <= 1;
                    Btns <= 0;
                    score <= 0;
                    for(i=0;i<16;i=i+1) board[i]<=0;
                    for(i=0;i<4;i=i+1) splitScore[i]<=0;
                    for(i=0;i<3;i=i+1) alphabet[i]<=0;
                    board_init <= 1; 
                    finishCheck <= 0;
                    packet <= {58'd0, 2'd0, leftBtn, downBtn, upBtn, rightBtn};
                end
                
                GAMESTART: begin
                    //init
                    score <= 0;
                    obst_cnt <= 0;
                    for(i=0;i<16;i=i+1) board[i]<=0;
                    for(i=0;i<4;i=i+1) splitScore[i]<=0;
                    successed <= 0;
                    changed <= 1;
                    //make obst
                    if(board_init == 1) begin
                        obst <= rnd[11:8];
                        board_init <= 0; 
                        obst_succ <= 1;
                    end
                    where <= rnd[15:12];
                    //assign
                    packet <= {3'd0, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],
                                2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                GAMEMAKE: begin
                
                    Btns <= {leftBtn, downBtn, upBtn, rightBtn};
                    //check finish
                    if ((board[0] !== board[1]) && (board[0] !== board[4]) && (board[1] !== board[5]) && (board[1] !== board[2]) &&(board[2] !== board[6]) &&
                            (board[2] !== board[3]) && (board[3] !== board[7]) && (board[4] !== board[5]) && (board[4] !== board[8]) && (board[5] !== board[9]) && 
                            (board[5] !== board[6]) && (board[6] !== board[7]) && (board[6] !== board[10]) && (board[7] !== board[11]) && (board[8] !== board[9]) && 
                            (board[8] !== board[12]) && (board[9] !== board[10]) && (board[9] !== board[13]) && (board[10] !== board[14]) && (board[10] !== board[11]) && 
                            (board[11] !== board[15]) && (board[12] !== board[13]) && (board[13] !== board[14]) && (board[14] !== board[15])&&(!((board[0]==0)&&(obst!=0)))&&
                            (!((board[1]==0)&&(obst!=1)))&&(!((board[2]==0)&&(obst!=2)))&&(!((board[3]==0)&&(obst!=3)))&&(!((board[4]==0)&&(obst!=4)))&&
                            (!((board[5]==0)&&(obst!=5)))&&(!((board[6]==0)&&(obst!=6)))&&(!((board[7]==0)&&(obst!=7)))&&(!((board[8]==0)&&(obst!=8)))&&
                            (!((board[9]==0)&&(obst!=9)))&&(!((board[10]==0)&&(obst!=10)))&&(!((board[11]==0)&&(obst!=11)))&&(!((board[12]==0)&&(obst!=12)))&&
                            (!((board[13]==0)&&(obst!=13)))&&(!((board[14]==0)&&(obst!=14)))&&(!((board[15]==0)&&(obst!=15))))  
                        finishCheck <= 2;
                    for (i = 0;i < 16; i = i + 1) if (board[i]==7) finishCheck <= 1;
                    score <= splitScore[0] + splitScore[1] + splitScore[2] + splitScore[3];
                    if(((board[0]!=0)||(obst==0)) && ((board[1]!=0)||(obst==1)) && ((board[2]!=0)||(obst==2)) && ((board[3]!=0)||(obst==3)) && 
                        ((board[4]!=0)||(obst==4)) && ((board[5]!=0)||(obst==5)) && ((board[6]!=0)||(obst==6)) && ((board[7]!=0)||(obst==7)) && 
                        ((board[8]!=0)||(obst==8)) && ((board[9]!=0)||(obst==9)) && ((board[10]!=0)||(obst==10)) && ((board[11]!=0)||(obst==11)) && 
                        ((board[12]!=0)||(obst==12)) && ((board[13]!=0)||(obst==13)) && ((board[14]!=0)||(obst==14)) && 
                        ((board[15]!=0)||(obst==15))) begin obst_succ <= 1; obst_cnt <= 7; end
                    else if ((obst_succ == 0) && (board[rnd[11:8]]==0) && (obst!=rnd[11:8])) begin
                        obst <= rnd[11:8];
                        obst_succ <= 1;
                    end 
                    //new Block
                    where <= rnd[15:12];
                    if((board[where] == 0) && (obst!=where) && (successed == 0) && (changed == 1) && (obst_succ == 1)) begin 
                        board[where] <= ((rnd[7:4]%10) == 1)?2:1; 
                        successed <= 1; changed <= 0; end
                    //assign
                    packet <= {3'd0, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],
                                2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                GAMEMOVE: begin
                    if (obst_cnt == 7) begin
                        obst_succ <= 0;
                        obst_cnt <= 1;
                    end
                    else obst_cnt <= obst_cnt + 1;
                    case(Btns)
                    4'b1000: begin //left
                            for (j=0;j<4;j=j+1) begin
                                if((board[3+4*j]==0) && (board[2+4*j]==0) && (board[1+4*j]==0) && (board[0+4*j]==0)) begin end
                                else if ((obst==(0+j*4))||(board[0+j*4]!=0)) begin
                                    if(((obst==(1+j*4))||(board[1+j*4]!=0)) && (board[2+j*4]==0)) begin
                                        board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                            if(board[3+j*4]!=0) changed <= 1; end
                                    else if((board[1+j*4]==0)&&(obst!=(1+j*4))) begin
                                        if ((board[2+j*4]==0)&&(obst!=(2+j*4))) begin
                                            board[1+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                                if(board[3+j*4]!=0) changed <= 1; end
                                        else if (board[2+j*4]!=0) begin
                                            board[1+j*4] <= board[2+j*4]; board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                                changed <= 1; end   
                                    end
                                end
                                
                                else if ((board[0+j*4]==0) && (obst != (0+j*4))) begin
                                    if(board[1+j*4] != 0) begin
                                        board[0+j*4] <= board[1+j*4]; changed <= 1; 
                                        if(obst==(2+j*4)) board[1+j*4] <= 0;
                                        else if (board[2+j*4]==0) begin board[1+j*4] <= board[3+j*4]; board[3+j*4] <= 0; end
                                        else begin board[1+j*4] <= board[2+j*4]; board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0; end
                                    end
                                    else if ((obst!=(1+j*4))&&(board[1+4*j]==0)) begin
                                        if (board[2+4*j] != 0) begin changed <= 1; 
                                            board[0+j*4] <= board[2+j*4]; board[1+j*4] <= board[3+j*4]; board[2+j*4] <= 0; board[3+j*4] <= 0; end
                                        else if ((obst!=(2+j*4))&&(board[2+4*j]==0)) begin board[0+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                            if(board[3+j*4]!=0) changed <= 1; end
                                    end
                                    else if ((obst==(1+j*4)) && (board[2+4*j]==0)) begin board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                        if(board[3+j*4]!=0) changed <= 1; end
                                end 
                            end
                        end
                    4'b0001: begin //right
                        for (j=0;j<4;j=j+1) begin
                            if((board[0+4*j]==0) && (board[1+4*j]==0) && (board[2+4*j]==0) && (board[3+4*j]==0)) begin end
                            else if ((obst==(3+j*4))||(board[3+j*4]!=0)) begin
                                if(((obst==(2+j*4))||(board[2+j*4]!=0)) && (board[1+j*4]==0)) begin
                                    board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                        if(board[0+j*4]!=0) changed <= 1; end
                                else if((board[2+j*4]==0)&&(obst!=(2+j*4))) begin
                                    if ((board[1+j*4]==0)&&(obst!=(1+j*4))) begin
                                        board[2+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                            if(board[0+j*4]!=0) changed <= 1; end
                                    else if (board[1+j*4]!=0) begin
                                        board[2+j*4] <= board[1+j*4]; board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                            changed <= 1; end   
                                    end
                            end                
                            else if ((board[3+j*4]==0) && (obst != (3+j*4))) begin
                                if(board[2+j*4] != 0) begin
                                    board[3+j*4] <= board[2+j*4]; changed <= 1; 
                                    if(obst==(1+j*4)) board[2+j*4] <= 0;
                                    else if (board[1+j*4]==0) begin board[2+j*4] <= board[0+j*4]; board[0+j*4] <= 0; end
                                    else begin board[2+j*4] <= board[1+j*4]; board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0; end
                                end
                                else if ((obst!=(2+j*4))&&(board[2+4*j]==0)) begin
                                    if (board[1+4*j] != 0) begin changed <= 1; 
                                        board[3+j*4] <= board[1+j*4]; board[2+j*4] <= board[0+j*4]; board[1+j*4] <= 0; board[0+j*4] <= 0; end
                                    else if ((obst!=(1+j*4))&&(board[1+4*j]==0)) begin board[3+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                        if(board[0+j*4]!=0) changed <= 1; end
                                end
                                else if ((obst==(2+j*4)) && (board[1+4*j]==0)) begin board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                    if(board[0+j*4]!=0) changed <= 1; end
                            end 
                        end
                    end
                    4'b0010: begin //up
                            for (j=0;j<4;j=j+1) begin
                                if((board[3*4+j]==0) && (board[2*4+j]==0) && (board[1*4+j]==0) && (board[0*4+j]==0)) begin end
                                else if ((obst==(0*4+j))||(board[0*4+j]!=0)) begin
                                    if(((obst==(1*4+j))||(board[1*4+j]!=0)) && (board[2*4+j]==0)) begin
                                        board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                            if(board[3*4+j]!=0) changed <= 1; end
                                    else if((board[1*4+j]==0)&&(obst!=(1*4+j))) begin
                                        if ((board[2*4+j]==0)&&(obst!=(2*4+j))) begin
                                            board[1*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                                if(board[3*4+j]!=0) changed <= 1; end
                                        else if (board[2*4+j]!=0) begin
                                            board[1*4+j] <= board[2*4+j]; board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                                changed <= 1; end   
                                    end
                                end
                                
                                else if ((board[0*4+j]==0) && (obst != (0*4+j))) begin
                                    if(board[1*4+j] != 0) begin
                                        board[0*4+j] <= board[1*4+j]; changed <= 1; 
                                        if(obst==(2*4+j)) board[1*4+j] <= 0;
                                        else if (board[2*4+j]==0) begin board[1*4+j] <= board[3*4+j]; board[3*4+j] <= 0; end
                                        else begin board[1*4+j] <= board[2*4+j]; board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0; end
                                    end
                                    else if ((obst!=(1*4+j))&&(board[1*4+j]==0)) begin
                                        if (board[2*4+j] != 0) begin changed <= 1; 
                                            board[0*4+j] <= board[2*4+j]; board[1*4+j] <= board[3*4+j]; board[2*4+j] <= 0; board[3*4+j] <= 0; end
                                        else if ((obst!=(2*4+j))&&(board[2*4+j]==0)) begin board[0*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                            if(board[3*4+j]!=0) changed <= 1; end
                                    end
                                    else if ((obst==(1*4+j)) && (board[2*4+j]==0)) begin board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                        if(board[3*4+j]!=0) changed <= 1; end
                                end 
                            end
                        end
                    4'b0100: begin //down
                        for (j=0;j<4;j=j+1) begin
                            if((board[0*4+j]==0) && (board[1*4+j]==0) && (board[2*4+j]==0) && (board[3*4+j]==0)) begin end
                            else if ((obst==(3*4+j))||(board[3*4+j]!=0)) begin
                                if(((obst==(2*4+j))||(board[2*4+j]!=0)) && (board[1*4+j]==0)) begin
                                    board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                        if(board[0*4+j]!=0) changed <= 1; end
                                else if((board[2*4+j]==0)&&(obst!=(2*4+j))) begin
                                    if ((board[1*4+j]==0)&&(obst!=(1*4+j))) begin
                                        board[2*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                            if(board[0*4+j]!=0) changed <= 1; end
                                    else if (board[1*4+j]!=0) begin
                                        board[2*4+j] <= board[1*4+j]; board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                            changed <= 1; end   
                                    end
                            end                
                            else if ((board[3*4+j]==0) && (obst != (3*4+j))) begin
                                if(board[2*4+j] != 0) begin
                                    board[3*4+j] <= board[2*4+j]; changed <= 1; 
                                    if(obst==(1*4+j)) board[2*4+j] <= 0;
                                    else if (board[1*4+j]==0) begin board[2*4+j] <= board[0*4+j]; board[0*4+j] <= 0; end
                                    else begin board[2*4+j] <= board[1*4+j]; board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0; end
                                end
                                else if ((obst!=(2*4+j))&&(board[2*4+j]==0)) begin
                                    if (board[1*4+j] != 0) begin changed <= 1; 
                                        board[3*4+j] <= board[1*4+j]; board[2*4+j] <= board[0*4+j]; board[1*4+j] <= 0; board[0*4+j] <= 0; end
                                    else if ((obst!=(1*4+j))&&(board[1*4+j]==0)) begin board[3*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                        if(board[0*4+j]!=0) changed <= 1; end
                                end
                                else if ((obst==(2*4+j)) && (board[1*4+j]==0)) begin board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                    if(board[0*4+j]!=0) changed <= 1; end
                            end 
                        end
                    end 
                    default: begin end
                    endcase
                    packet <= {3'd0, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                GAMEMERGE: begin
                    case(Btns)
                        4'b1000: begin //left
                        for(j=0;j<4;j=j+1) begin
                            if((board[0+4*j] == board[1+4*j]) && (board[0+4*j]!=0)) begin
                                    board[0+4*j] <= board[0+4*j] + 1; changed <= 1;
                                    if((board[2+4*j] == board[3+4*j]) && (board[2+4*j]!=0)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0+4*j] + 1)) + (1<<(board[2+4*j] + 1));
                                        board[1+4*j] <= board[2+4*j] + 1; board[2+4*j]<=0; board[3+4*j]<=0; end
                                    else if((board[2+4*j]==0)&&(obst!=(2+4*j))) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0+4*j] + 1));
                                        if (board[3+4*j]==0) board[1+4*j] <= 0;
                                        else begin board[1+4*j] <= board[3+4*j]; board[3+4*j]<=0; end
                                    end
                                    
                                    else if(obst==(2+4*j)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0+4*j] + 1));
                                        board[1+4*j] <= 0; end
                                    else begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0+4*j] + 1));
                                        board[1+4*j] <= board[2+4*j]; board[2+4*j] <= board[3+4*j]; board[3+4*j] <= 0; end
                            end
                            
                            else if ((board[1+4*j] == board[2+4*j]) && (board[1+4*j]!=0)) begin
                                board[1+4*j] <= board[1+4*j] + 1; board[2+4*j] <= board[3+4*j]; board[3+4*j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[1+4*j] + 1)); changed <= 1; end
                            else if ((board[2+4*j] == board[3+4*j]) && (board[2+4*j]!=0)) begin
                                board[2+4*j] <= board[2+4*j] + 1; board[3+4*j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[2+4*j] + 1)); changed <= 1; end
                        end end
                        4'b0001: begin //right
                        for(j=0;j<4;j=j+1) begin
                            if((board[3+4*j] == board[2+4*j]) && (board[3+4*j]!=0)) begin
                                    board[3+4*j] <= board[3+4*j] + 1; changed <= 1;
                                    if((board[1+4*j] == board[0+4*j]) && (board[1+4*j]!=0)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3+4*j] + 1)) + (1<<(board[1+4*j] + 1));
                                        board[2+4*j] <= board[1+4*j] + 1; board[1+4*j]<=0; board[0+4*j]<=0; end
                                    else if((board[1+4*j]==0)&&(obst!=(1+4*j))) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3+4*j] + 1));
                                        if (board[0+4*j]==0) board[2+4*j] <= 0;
                                        else begin board[2+4*j] <= board[0+4*j]; board[0+4*j]<=0; end
                                    end
                                    else if(obst==(1+4*j)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3+4*j] + 1));
                                        board[2+4*j] <= 0; end
                                    else begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3+4*j] + 1));
                                        board[2+4*j] <= board[1+4*j]; board[1+4*j] <= board[0+4*j]; board[0+4*j] <= 0; end
                            end
                            else if ((board[2+4*j] == board[1+4*j]) && (board[2+4*j]!=0)) begin
                                board[2+4*j] <= board[2+4*j] + 1; board[1+4*j] <= board[0+4*j]; board[0+4*j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[2+4*j] + 1)); changed <= 1; end
                            else if ((board[1+4*j] == board[0+4*j]) && (board[1+4*j]!=0)) begin
                                board[1+4*j] <= board[1+4*j] + 1; board[0+4*j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[1+4*j] + 1)); changed <= 1; end
                        end end
                        4'b0010: begin //up
                        for(j=0;j<4;j=j+1) begin
                            if((board[0*4+j] == board[1*4+j]) && (board[0*4+j]!=0)) begin
                                    board[0*4+j] <= board[0*4+j] + 1; changed <= 1;
                                    if((board[2*4+j] == board[3*4+j]) && (board[2*4+j]!=0)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0*4+j] + 1)) + (1<<(board[2*4+j] + 1));
                                        board[1*4+j] <= board[2*4+j] + 1; board[2*4+j]<=0; board[3*4+j]<=0; end
                                    else if((board[2*4+j]==0)&&(obst!=(2*4+j))) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0*4+j] + 1));
                                        if (board[3*4+j]==0) board[1*4+j] <= 0;
                                        else begin board[1*4+j] <= board[3*4+j]; board[3*4+j]<=0; end
                                    end
                                    else if(obst==(2*4+j)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0*4+j] + 1));
                                        board[1*4+j] <= 0; end
                                    else begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[0*4+j] + 1));
                                        board[1*4+j] <= board[2*4+j]; board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0; end
                            end
                            else if ((board[1*4+j] == board[2*4+j]) && (board[1*4+j]!=0)) begin
                                board[1*4+j] <= board[1*4+j] + 1; board[2*4+j] <= board[3*4+j]; board[3*4+j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[1*4+j] + 1)); changed <= 1; end
                            else if ((board[2*4+j] == board[3*4+j]) && (board[2*4+j]!=0)) begin
                                board[2*4+j] <= board[2*4+j] + 1; board[3*4+j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[2*4+j] + 1)); changed <= 1; end
                        end end
                        4'b0100: begin //down
                        for(j=0;j<4;j=j+1) begin
                            if((board[3*4+j] == board[2*4+j]) && (board[3*4+j]!=0)) begin
                                    board[3*4+j] <= board[3*4+j] + 1; changed <= 1;
                                    if((board[1*4+j] == board[0*4+j]) && (board[1*4+j]!=0)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3*4+j] + 1)) + (1<<(board[1*4+j] + 1));
                                        board[2*4+j] <= board[1*4+j] + 1; board[1*4+j]<=0; board[0*4+j]<=0; end
                                    else if((board[1*4+j]==0)&&(obst!=(1*4+j))) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3*4+j] + 1));
                                        if (board[0*4+j]==0) board[2*4+j] <= 0;
                                        else begin board[2*4+j] <= board[0*4+j]; board[0*4+j]<=0; end
                                    end
                                    else if(obst==(1*4+j)) begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3*4+j] + 1));
                                        board[2*4+j] <= 0; end
                                    else begin
                                        splitScore[j] <= splitScore[j] + (1<<(board[3*4+j] + 1));
                                        board[2*4+j] <= board[1*4+j]; board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0; end
                            end
                            else if ((board[2*4+j] == board[1*4+j]) && (board[2*4+j]!=0)) begin
                                board[2*4+j] <= board[2*4+j] + 1; board[1*4+j] <= board[0*4+j]; board[0*4+j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[2*4+j] + 1)); changed <= 1; end
                            else if ((board[1*4+j] == board[0*4+j]) && (board[1*4+j]!=0)) begin
                                board[1*4+j] <= board[1*4+j] + 1; board[0*4+j]<=0;
                                splitScore[j] <= splitScore[j] + (1<<(board[1*4+j] + 1)); changed <= 1; end
                        end end
                        default: begin end
                    endcase
                    if(changed==1) successed <= 0;
                    packet <= {3'd0, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],2'd2,leftBtn, downBtn, upBtn, rightBtn};

                end
                GAMEEND: begin
                    for(i=0;i<16;i=i+1) board[i]<=0;
                    for(i=0;i<4;i=i+1) splitScore[i]<=0;
                    alpha <= 0;
                    packet <= {3'd0, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],
                                2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                RANKREGI: begin
                    scoreNo <=1;
                    if ((alpha < 2)&&(rightBtn==1))
                        alpha <= alpha + 1;
                        
                    if(next_state == RANKVIEW) begin alpha <= 0;
                        if (score >= scoreBoard[0]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=scoreBoard[2]; 
                            scoreBoard[2]<=scoreBoard[1]; scoreBoard[1]<=scoreBoard[0]; scoreBoard[0]<=score; 
                            name[4]<=name[3]; name[3]<=name[2]; name[2]<=name[1]; name[1]<=name[0];
                            name[0]<={alphabet[2], alphabet[1], alphabet[0]};   end
                        else if (score >= scoreBoard[1]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=scoreBoard[2]; 
                            scoreBoard[2]<=scoreBoard[1]; scoreBoard[1]<=score; 
                            name[4]<=name[3]; name[3]<=name[2]; name[2]<=name[1]; name[1]<={alphabet[2], alphabet[1], alphabet[0]}; end
                        else if (score >= scoreBoard[2]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=scoreBoard[2]; scoreBoard[2]<=score; 
                            name[4]<=name[3]; name[3]<=name[2]; name[2]<={alphabet[2], alphabet[1], alphabet[0]}; end
                        else if (score >= scoreBoard[3]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=score; 
                            name[4]<=name[3]; name[3]<={alphabet[2], alphabet[1], alphabet[0]}; end
                        else if (score >= scoreBoard[4]) begin scoreBoard[4]<=score; name[4]<={alphabet[2], alphabet[1], alphabet[0]}; end
                    end
                    else begin
                        if (downBtn==1) 
                            if (alphabet[alpha] == 0) alphabet[alpha] <= 25;
                            else alphabet[alpha] <= (alphabet[alpha] - 1)%26;
                        else if (upBtn==1)
                            alphabet[alpha] <= (alphabet[alpha] + 1)%26;
                    end
                    packet <= {state, 23'd0,score,alphabet[2], alphabet[1], alphabet[0], 2'b11,leftBtn, downBtn, upBtn, rightBtn};
                end
                
                RANKVIEW: begin
                    if (downBtn==1) 
                        if (scoreNo==1) scoreNo <= 5;
                        else scoreNo <= (scoreNo - 1)%5;
                    else if (upBtn==1)
                        if (scoreNo==4) scoreNo <= 5;
                        else scoreNo <= (scoreNo + 1)%5;
                    
                    packet <= {state, 20'd0,scoreNo,scoreBoard[scoreNo-1],name[scoreNo-1],2'b01,leftBtn, downBtn, upBtn, rightBtn};
                end
                
           endcase
        end
    end
    
    always @(state or start or leftBtn or rightBtn or upBtn or downBtn) begin
        
        casez ({state, start, {leftBtn, downBtn, upBtn, rightBtn}}) 
            {4'bz, 1'b0, 4'b0000}: next_state = IDLE;
            {IDLE, 1'b1, 4'b0000}: next_state = MENU;
            {MENU, 1'b1, 4'b1000}: begin next_state = RANKVIEW; end
            {MENU, 1'b1, 4'b0001}: begin next_state = GAMESTART; end
            {RANKVIEW, 1'b1, 4'b0001}: begin next_state = MENU; end
            {RANKVIEW, 1'b1, 4'bzzz0}: begin next_state = RANKVIEW; end
            {GAMESTART, 1'b1, 4'bz}: begin next_state = GAMEMAKE; end
            {GAMEMAKE, 1'b1, 4'bz}: begin
                // check finish
                if (finishCheck > 0) next_state = GAMEEND;
                else if ((leftBtn|downBtn|upBtn|rightBtn) && successed==1) begin next_state = GAMEMOVE; end
                else next_state = GAMEMAKE;
            end
            {GAMEMOVE, 1'b1, 4'bz}: begin next_state = GAMEMERGE; end
            {GAMEMERGE, 1'b1, 4'bz}: begin next_state = GAMEMAKE; end
            {GAMEEND, 1'b1, 4'b0001}: begin next_state = MENU; end
            {GAMEEND, 1'b1, 4'b1000}: begin next_state = RANKREGI; end
            {RANKREGI, 1'b1, 4'b0001}:  begin if(alpha == 2) next_state = RANKVIEW; else next_state = RANKREGI; end
            
            default: begin next_state = state; end
        endcase
    end
    
endmodule


//fpga4student.com: FPGA projects, Verilog projects, VHDL projects
// Verilog code for button debouncing on FPGA
// debouncing module without creating another clock domain
// by using clock enable signal 
module debounce_better_version(input pb_1,clk,output pb_out);
    wire Q1,Q2,Q2_bar,Q0;
    my_dff_en d0(clk,pb_1,Q0);
    
    my_dff_en d1(clk,Q0,Q1);
    my_dff_en d2(clk,Q1,Q2);
    assign Q2_bar = ~Q2;
    assign pb_out = Q1 & Q2_bar;
endmodule
// D-flip-flop with clock enable signal for debouncing module 
module my_dff_en(input DFF_CLOCK,D, output reg Q=0);
    always @ (posedge DFF_CLOCK) begin
           Q <= D;
    end
endmodule 

`timescale 1ns / 1ps
`define CLK_PERIOD 10

module top_tb();
    reg clk;
    reg rstn;
    reg [1:0] sw;
    reg [3:0] btn;
    wire [63:0] packet;
    
    top UUT(
        .clk(clk),
        .rstn(rstn),
        .sw(sw),
        .btn(btn),
        .packet(packet)
    );
    
    
    initial begin
        clk = 0;
        sw = 0;
        btn = 0;
        rstn = 1;
        #2; rstn = 0;
        #2; rstn = 1;
        
        sw[1] = 1;
        
        #10; btn = 4'b1000;
        #10; btn = 4'b0000;
        #5; btn = 4'b0000;
        
        #10;#10;#10; btn = 4'b1000;
        #10;#10;#10; btn = 4'b0000;
        #10;#10;#10; btn = 4'b1000;
        #10;#10;#10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0010;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        #10; btn = 4'b0100;
        #10; btn = 4'b0000;
        
        
        
        
        
    end
    
    always #(`CLK_PERIOD/2) clk = ~clk;
    
    event testbench_finish;
    initial #(`CLK_PERIOD*150) -> testbench_finish;
    //always @(*) begin
    //    if (result > 0) -> testbench_finish;
    //end
    
    always @(testbench_finish) begin
        $display("The testbench is finished...");
        $finish;
    end
    
endmodule

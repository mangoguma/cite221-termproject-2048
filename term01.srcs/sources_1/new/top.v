`timescale 1ns / 1ps

module top(
        input wire clk,
        input wire rstn,
        input wire [1:0] sw,
        input wire [3:0] btn,
        output reg [63:0] packet        
    );
    reg [2:0] scoreNo = 0;
    reg [54:0] newPacket;
    reg [3:0] state, next_state, Btns, obst, board[15:0];
    reg [15:0] score, splitScore[3:0], scoreBoard[4:0];
    reg [1:0] finishCheck = 0;
    
    wire [15:0] rnd;
    wire start, leftBtn, downBtn, upBtn, rightBtn;
    assign start = sw[1];
    assign leftBtn = btn[3];
    assign downBtn = btn[2];
    assign upBtn = btn[1];
    assign rightBtn = btn[0];
    
    //parameter for state
    parameter IDLE = 0, MENU = 1, RANKVIEW = 2, RANKREGI = 3;
    parameter GAMESTART = 4, GAMEMAKE = 5, GAMEEND = 8, GAMEMOVE = 6, GAMEMERGE = 7;
    integer i, j, di, dj, where, board_init=0, countEmpty, 
        obst_cnt = 0, changed = 1, cntM, successed = 0;
    random rand (.clk(clk), .rstn(rstn), .rnd(rnd));
    
    always @(negedge rstn or posedge clk) begin
        if (~rstn) begin
            packet <= 0;
            state <= 0;
            Btns <= 0;
            score <= 0;
            finishCheck <= 0;
            for(i=0;i<16;i=i+1) board[i]<=0;
            for(i=0;i<4;i=i+1) splitScore[i]<=0;
            for(i=0;i<5;i=i+1) scoreBoard[i]<=0;
        end
        else begin
            state <= next_state;
            case(state)
                IDLE: begin packet <= {state, 54'd0, 2'd0, leftBtn, downBtn, upBtn, rightBtn}; end
                MENU: begin
                    scoreNo <= 0;
                    Btns <= 0;
                    score <= 0;
                    for(i=0;i<16;i=i+1) board[i]<=0;
                    for(i=0;i<4;i=i+1) splitScore[i]<=0;
                    board_init <= 1; 
                    finishCheck <= 0;
                    packet <= {state, 54'd0, 2'd0, leftBtn, downBtn, upBtn, rightBtn};
                end
                
                GAMESTART: begin
                    successed <= 0;
                    changed <= 1;
                    //make obst
                    if(board_init == 1) begin
                        obst <= rnd[11:8];
                        board_init <= 0; 
                    end
                    where <= rnd[15:12];
                    //assign
                    packet <= {3'd1, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                GAMEMAKE: begin
                    Btns <= {leftBtn, downBtn, upBtn, rightBtn};
                    //check finish
                    if ((board[0] !== board[1]) && (board[0] !== board[4]) && (board[1] !== board[5]) && (board[1] !== board[2]) &&(board[2] !== board[6]) &&
                            (board[2] !== board[3]) && (board[3] !== board[7]) && (board[4] !== board[5]) && (board[4] !== board[8]) && (board[5] !== board[9]) && 
                            (board[5] !== board[6]) && (board[6] !== board[7]) && (board[6] !== board[10]) && (board[7] !== board[11]) && (board[8] !== board[9]) && 
                            (board[8] !== board[12]) && (board[9] !== board[10]) && (board[9] !== board[13]) && (board[10] !== board[14]) && (board[10] !== board[11]) && 
                            (board[11] !== board[15]) && (board[12] !== board[13]) && (board[13] !== board[14]) && (board[14] !== board[15])&&(!((board[0]==0)&&(obst!=0)))&&
                            (!((board[1]==0)&&(obst!=1)))&&(!((board[2]==0)&&(obst!=2)))&&(!((board[3]==0)&&(obst!=3)))&&(!((board[4]==0)&&(obst!=4)))&&
                            (!((board[5]==0)&&(obst!=5)))&&(!((board[6]==0)&&(obst!=6)))&&(!((board[7]==0)&&(obst!=7)))&&(!((board[8]==0)&&(obst!=8)))&&
                            (!((board[9]==0)&&(obst!=9)))&&(!((board[10]==0)&&(obst!=10)))&&(!((board[11]==0)&&(obst!=11)))&&(!((board[12]==0)&&(obst!=12)))&&
                            (!((board[13]==0)&&(obst!=13)))&&(!((board[14]==0)&&(obst!=14)))&&(!((board[15]==0)&&(obst!=15))))  
                        finishCheck <= 1;
                    for (i = 0;i < 16; i = i + 1) if ((board[i]==7) && (i!=obst)) finishCheck <= 2;
                    //new Block
                    where <= rnd[15:12];
                    if((board[where] == 0) && (obst!=where) && (successed == 0) && (changed == 1)) begin 
                        board[where] <= ((rnd[7:4]%10) == 1)?2:1; 
                        successed <= 1; changed <= 0; end
                    //assign
                    packet <= {3'd2, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],
                                2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                GAMEMOVE: begin
                    case(Btns)
                    4'b1000: begin //left
                            for (j=0;j<4;j=j+1) begin
                                if((board[3+4*j]==0) && (board[2+4*j]==0) && (board[1+4*j]==0) && (board[0+4*j]==0)) begin end
                                else if ((obst==(0+j*4))||(board[0+j*4]!=0)) begin
                                    if(((obst==(1+j*4))||(board[1+j*4]!=0)) && (board[2+j*4]==0)) begin
                                        board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                            if(board[3+j*4]!=0) changed <= 1; end
                                    else if((board[1+j*4]==0)&&(obst!=(1+j*4))) begin
                                        if ((board[2+j*4]==0)&&(obst!=(2+j*4))) begin
                                            board[1+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                                if(board[3+j*4]!=0) changed <= 1; end
                                        else if (board[2+j*4]!=0) begin
                                            board[1+j*4] <= board[2+j*4]; board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                                changed <= 1; end   
                                        else if ((board[2+j*4]==0)&&(obst!=(2+j*4)) && (board[3+j*4]!=0)) begin
                                            board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                                changed <= 1; end
                                    end
                                end
                                
                                else if ((board[0+j*4]==0) && (obst != (0+j*4))) begin
                                    if(board[1+j*4] != 0) begin
                                        board[0+j*4] <= board[1+j*4]; changed <= 1; 
                                        if(obst==(2+j*4)) board[1+j*4] <= 0;
                                        else if (board[2+j*4]==0) begin board[1+j*4] <= board[3+j*4]; board[3+j*4] <= 0; end
                                        else begin board[1+j*4] <= board[2+j*4]; board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0; end
                                    end
                                    else if ((obst!=(1+j*4))&&(board[1+4*j]==0)) begin
                                        if (board[2+4*j] != 0) begin changed <= 1; 
                                            board[0+j*4] <= board[2+j*4]; board[1+j*4] <= board[3+j*4]; board[2+j*4] <= 0; board[3+j*4] <= 0; end
                                        else if ((obst!=(2+j*4))&&(board[2+4*j]==0)) begin board[0+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                            if(board[3+j*4]!=0) changed <= 1; end
                                    end
                                    else if ((obst==(1+j*4)) && (board[2+4*j]==0)) begin board[2+j*4] <= board[3+j*4]; board[3+j*4] <= 0;
                                        if(board[3+j*4]!=0) changed <= 1; end
                                end 
                            end
                        end
                        4'b0001: begin //right
                            for (j=0;j<4;j=j+1) begin
                                if((board[0+4*j]==0) && (board[1+4*j]==0) && (board[2+4*j]==0) && (board[3+4*j]==0)) begin end
                                else if ((obst==(3+j*4))||(board[3+j*4]!=0)) begin
                                    if(((obst==(2+j*4))||(board[2+j*4]!=0)) && (board[1+j*4]==0)) begin
                                        board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                            if(board[0+j*4]!=0) changed <= 1; end
                                    else if((board[2+j*4]==0)&&(obst!=(2+j*4))) begin
                                        if ((board[1+j*4]==0)&&(obst!=(1+j*4))) begin
                                            board[2+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                                if(board[0+j*4]!=0) changed <= 1; end
                                        else if (board[1+j*4]!=0) begin
                                            board[2+j*4] <= board[1+j*4]; board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                                changed <= 1; end   
                                        else if ((board[1+j*4]==0)&&(obst!=(1+j*4)) && (board[0+j*4]!=0)) begin
                                            board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                                changed <= 1; end
                                        end
                                end                
                                else if ((board[3+j*4]==0) && (obst != (3+j*4))) begin
                                    if(board[2+j*4] != 0) begin
                                        board[3+j*4] <= board[2+j*4]; changed <= 1; 
                                        if(obst==(1+j*4)) board[2+j*4] <= 0;
                                        else if (board[1+j*4]==0) begin board[2+j*4] <= board[0+j*4]; board[0+j*4] <= 0; end
                                        else begin board[2+j*4] <= board[1+j*4]; board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0; end
                                    end
                                    else if ((obst!=(2+j*4))&&(board[2+4*j]==0)) begin
                                        if (board[1+4*j] != 0) begin changed <= 1; 
                                            board[3+j*4] <= board[1+j*4]; board[2+j*4] <= board[0+j*4]; board[1+j*4] <= 0; board[0+j*4] <= 0; end
                                        else if ((obst!=(1+j*4))&&(board[1+4*j]==0)) begin board[3+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                            if(board[0+j*4]!=0) changed <= 1; end
                                    end
                                    else if ((obst==(2+j*4)) && (board[1+4*j]==0)) begin board[1+j*4] <= board[0+j*4]; board[0+j*4] <= 0;
                                        if(board[0+j*4]!=0) changed <= 1; end
                                end 
                            end
                        end
                        
                    4'b0010: begin //up
                            for (j=0;j<4;j=j+1) begin
                                if((board[3*4+j]==0) && (board[2*4+j]==0) && (board[1*4+j]==0) && (board[0*4+j]==0)) begin end
                                else if ((obst==(0*4+j))||(board[0*4+j]!=0)) begin
                                    if(((obst==(1*4+j))||(board[1*4+j]!=0)) && (board[2*4+j]==0)) begin
                                        board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                            if(board[3*4+j]!=0) changed <= 1; end
                                    else if((board[1*4+j]==0)&&(obst!=(1*4+j))) begin
                                        if ((board[2*4+j]==0)&&(obst!=(2*4+j))) begin
                                            board[1*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                                if(board[3*4+j]!=0) changed <= 1; end
                                        else if (board[2*4+j]!=0) begin
                                            board[1*4+j] <= board[2*4+j]; board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                                changed <= 1; end   
                                        else if ((board[2*4+j]==0)&&(obst!=(2*4+j)) && (board[3*4+j]!=0)) begin
                                            board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                                changed <= 1; end
                                    end
                                end
                                
                                else if ((board[0*4+j]==0) && (obst != (0*4+j))) begin
                                    if(board[1*4+j] != 0) begin
                                        board[0*4+j] <= board[1*4+j]; changed <= 1; 
                                        if(obst==(2*4+j)) board[1*4+j] <= 0;
                                        else if (board[2*4+j]==0) begin board[1*4+j] <= board[3*4+j]; board[3*4+j] <= 0; end
                                        else begin board[1*4+j] <= board[2*4+j]; board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0; end
                                    end
                                    else if ((obst!=(1*4+j))&&(board[1*4+j]==0)) begin
                                        if (board[2*4+j] != 0) begin changed <= 1; 
                                            board[0*4+j] <= board[2*4+j]; board[1*4+j] <= board[3*4+j]; board[2*4+j] <= 0; board[3*4+j] <= 0; end
                                        else if ((obst!=(2*4+j))&&(board[2*4+j]==0)) begin board[0*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                            if(board[3*4+j]!=0) changed <= 1; end
                                    end
                                    else if ((obst==(1*4+j)) && (board[2*4+j]==0)) begin board[2*4+j] <= board[3*4+j]; board[3*4+j] <= 0;
                                        if(board[3*4+j]!=0) changed <= 1; end
                                end 
                            end
                        end
                        4'b0100: begin //down
                            for (j=0;j<4;j=j+1) begin
                                if((board[0*4+j]==0) && (board[1*4+j]==0) && (board[2*4+j]==0) && (board[3*4+j]==0)) begin end
                                else if ((obst==(3*4+j))||(board[3*4+j]!=0)) begin
                                    if(((obst==(2*4+j))||(board[2*4+j]!=0)) && (board[1*4+j]==0)) begin
                                        board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                            if(board[0*4+j]!=0) changed <= 1; end
                                    else if((board[2*4+j]==0)&&(obst!=(2*4+j))) begin
                                        if ((board[1*4+j]==0)&&(obst!=(1*4+j))) begin
                                            board[2*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                                if(board[0*4+j]!=0) changed <= 1; end
                                        else if (board[1*4+j]!=0) begin
                                            board[2*4+j] <= board[1*4+j]; board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                                changed <= 1; end   
                                        else if ((board[1*4+j]==0)&&(obst!=(1*4+j)) && (board[0*4+j]!=0)) begin
                                            board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                                changed <= 1; end
                                        end
                                end                
                                else if ((board[3*4+j]==0) && (obst != (3*4+j))) begin
                                    if(board[2*4+j] != 0) begin
                                        board[3*4+j] <= board[2*4+j]; changed <= 1; 
                                        if(obst==(1*4+j)) board[2*4+j] <= 0;
                                        else if (board[1*4+j]==0) begin board[2*4+j] <= board[0*4+j]; board[0*4+j] <= 0; end
                                        else begin board[2*4+j] <= board[1*4+j]; board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0; end
                                    end
                                    else if ((obst!=(2*4+j))&&(board[2*4+j]==0)) begin
                                        if (board[1*4+j] != 0) begin changed <= 1; 
                                            board[3*4+j] <= board[1*4+j]; board[2*4+j] <= board[0*4+j]; board[1*4+j] <= 0; board[0*4+j] <= 0; end
                                        else if ((obst!=(1*4+j))&&(board[1*4+j]==0)) begin board[3*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                            if(board[0*4+j]!=0) changed <= 1; end
                                    end
                                    else if ((obst==(2*4+j)) && (board[1*4+j]==0)) begin board[1*4+j] <= board[0*4+j]; board[0*4+j] <= 0;
                                        if(board[0*4+j]!=0) changed <= 1; end
                                end 
                            end
                        end 
                        default: begin end
                    endcase
                    packet <= {state[2:0], finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],2'd2,leftBtn, downBtn, upBtn, rightBtn};

                end
                GAMEMERGE: begin
                    case(Btns)
                        4'b1000: begin //left
                            for(j=0;j<4;j=j+1) begin
                                if((board[0+j*4] == board[1+j*4]) && (board[0+4*j]!=0)) begin
                                    if((board[2+j*4] == board[3+j*4]) && (board[2+4*j]!=0)) begin
                                        board[0+j*4] <= board[0+j*4] + 1; board[1+j*4] <= board[2+j*4] + 1; board[2+j*4]<=0; board[3+j*4]<=0;
                                        splitScore[j] <= splitScore[j] + (1<<(board[0+j*4] + 1)) + (1<<(board[2+j*4] + 1)); changed <= 1; end  
                                    else begin
                                        board[0+j*4] <= board[0+j*4] + 1; board[1+j*4] <= board[2+j*4]; board[2+j*4] <= board[3+j*4]; board[3+j*4]<=0;
                                        splitScore[j] <= splitScore[j] + (1<<(board[0+j*4] + 1)); changed <= 1; end 
                                end
                                else if ((board[1+j*4] == board[2+j*4]) && (board[1+4*j]!=0)) begin
                                    board[1+j*4] <= board[1+j*4] + 1; board[2+j*4] <= board[3+j*4]; board[3+j*4]<=0;
                                    splitScore[j] <= splitScore[j] + (1<<(board[1+j*4] + 1)); changed <= 1; end
                                else if ((board[2+j*4] == board[3+j*4]) && (board[2+4*j]!=0)) begin
                                    board[2+j*4] <= board[2+j*4] + 1; board[3+j*4]<=0;
                                    splitScore[j] <= splitScore[j] + (1<<(board[2+j*4] + 1)); changed <= 1; end
                            end
                        end
                        4'b0001: begin //right
                            for(j=0;j<4;j=j+1) begin
                                if((board[3+j*4] == board[2+j*4]) && (board[3+4*j]!=0)) begin
                                    if((board[1+j*4] == board[0+j*4]) && (board[1+4*j]!=0)) begin
                                        board[3+j*4] <= board[3+j*4] + 1; board[2+j*4] <= board[1+j*4] + 1; board[1+j*4]<=0; board[0+j*4]<=0;
                                        splitScore[j] <= splitScore[j] + (1<<(board[3+j*4] + 1)) + (1<<(board[1+j*4] + 1)); changed <= 1; end  
                                    else begin
                                        board[3+j*4] <= board[3+j*4] + 1; board[2+j*4] <= board[1+j*4]; board[1+j*4] <= board[0+j*4]; board[0+j*4]<=0;
                                        splitScore[j] <= splitScore[j] + (1<<(board[3+j*4] + 1)); changed <= 1; end 
                                end
                                else if ((board[2+j*4] == board[1+j*4]) && (board[2+4*j]!=0)) begin
                                    board[2+j*4] <= board[2+j*4] + 1; board[1+j*4] <= board[0+j*4]; board[0+j*4]<=0;
                                    splitScore[j] <= splitScore[j] + (1<<(board[2+j*4] + 1)); changed <= 1; end
                                else if ((board[1+j*4] == board[0+j*4]) && (board[1+4*j]!=0)) begin
                                    board[1+j*4] <= board[1+j*4] + 1; board[0+j*4]<=0;
                                    splitScore[j] <= splitScore[j] + (1<<(board[1+j*4] + 1)); changed <= 1; end
                            end
                        end
                        4'b0010: begin //up
                            for(i=0;i<4;i=i+1) begin
                                if((board[i+0*4] == board[i+1*4]) && (board[i+0*4]!=0)) begin
                                    if((board[i+2*4] == board[i+3*4]) && (board[i+2*4]!=0)) begin
                                        board[i+0*4] <= board[i+0*4] + 1; board[i+4*1] <= board[i+4*2] + 1; board[i+4*2]<=0; board[i+3*4]<=0;
                                        splitScore[i] <= splitScore[i] + (1<<(board[i+0*4] + 1)) + (1<<(board[i+2*4] + 1)); changed = 1; end  
                                    else begin
                                        board[i+0*4] <= board[i+0*4] + 1; board[i+4*1] <= board[i+4*2]; board[i+4*2] <= board[i+3*4]; board[i+3*4]<=0;
                                        splitScore[i] <= splitScore[i] + (1<<(board[i+0*4] + 1)); changed = 1; end 
                                end
                                else if ((board[i+4*1] == board[i+4*2]) && (board[i+1*4]!=0)) begin
                                    board[i+4*1] <= board[i+4*1] + 1; board[i+4*2] <= board[i+3*4]; board[i+3*4]<=0;
                                    splitScore[i] <= splitScore[i] + (1<<(board[i+1*4] + 1)); changed <= 1; end
                                else if ((board[i+4*2] == board[i+3*4]) && (board[i+2*4]!=0)) begin
                                    board[i+4*2] <= board[i+4*2] + 1; board[i+3*4]<=0;
                                    splitScore[i] <= splitScore[i] + (1<<(board[i+2*4] + 1)); changed <= 1; end
                            end
                        end
                        4'b0100: begin //down
                            for(i=0;i<4;i=i+1) begin
                                if((board[i+3*4] == board[i+2*4]) && (board[i+3*4]!=0)) begin
                                    if((board[i+1*4] == board[i+0*4]) && (board[i+1*4]!=0)) begin
                                        board[i+3*4] <= board[i+3*4] + 1; board[i+4*2] <= board[i+4*1] + 1; board[i+4*1]<=0; board[i+0*4]<=0;
                                        splitScore[i] <= splitScore[i] + (1<<(board[i+3*4] + 1)) + (1<<(board[i+1*4] + 1)); changed <= 1; end  
                                    else begin
                                        board[i+3*4] <= board[i+3*4] + 1; board[i+4*2] <= board[i+4*1]; board[i+4*1] <= board[i+0*4]; board[i+0*4]<=0;
                                        splitScore[i] <= splitScore[i] + (1<<(board[i+3*4] + 1)); changed <= 1; end 
                                end
                                else if ((board[i+4*2] == board[i+4*1]) && (board[i+2*4]!=0)) begin
                                    board[i+4*2] <= board[i+4*2] + 1; board[i+4*1] <= board[i+0*4]; board[i+0*4]<=0;
                                    splitScore[i] <= splitScore[i] + (1<<(board[i+2*4] + 1)); changed <= 1; end
                                else if ((board[i+4*1] == board[i+0*4]) && (board[i+1*4]!=0)) begin
                                    board[i+4*1] <= board[i+4*1] + 1; board[i+0*4]<=0;
                                    splitScore[i] <= splitScore[i] + (1<<(board[i+1*4] + 1)); changed <= 1; end
                            end
                        end
                        default: begin end
                    endcase
                    if(changed==1) begin successed <= 0; obst_cnt <= obst_cnt + 1; end
                    packet <= {3'd3, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],2'd2,leftBtn, downBtn, upBtn, rightBtn};

                end
                GAMEEND: begin
                    score <= splitScore[0] + splitScore[1] + splitScore[2] + splitScore[3];  
                    for(i=0;i<16;i=i+1) board[i]<=0;
                    for(i=0;i<4;i=i+1) splitScore[i]<=0;
                    packet <= {3'd4, finishCheck,1'b1,obst,board[15],board[14],board[13],board[12],board[11],board[10],board[9],board[8],board[7],board[6],board[5],board[4],board[3],board[2],board[1],board[0],
                                2'd2,leftBtn, downBtn, upBtn, rightBtn};
                end
                RANKREGI: begin
                    scoreNo <=0;
                    if (score >= scoreBoard[0]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=scoreBoard[2]; scoreBoard[2]<=scoreBoard[1]; scoreBoard[1]<=scoreBoard[0]; scoreBoard[0]<=score; end
                    else if (score >= scoreBoard[1]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=scoreBoard[2]; scoreBoard[2]<=scoreBoard[1]; scoreBoard[1]<=score; end
                    else if (score >= scoreBoard[2]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=scoreBoard[2]; scoreBoard[2]<=score; end
                    else if (score >= scoreBoard[3]) begin scoreBoard[4]<=scoreBoard[3]; scoreBoard[3]<=score; end
                    else if (score >= scoreBoard[4]) begin scoreBoard[4]<=score; end
                    packet <= {state, 23'd0,score,15'd0,2'b11,leftBtn, downBtn, upBtn, rightBtn};
                end
                RANKVIEW: begin
                    case({upBtn, downBtn})
                        2'b01: scoreNo <= (scoreNo - 1)%5;
                        2'b10: scoreNo <= (scoreNo + 1)%5;
                        default: begin end
                    endcase
                    packet <= {state, 20'd0,scoreNo+1,scoreBoard[scoreNo],15'd0,2'b01,leftBtn, downBtn, upBtn, rightBtn};
                end
                
           endcase
        end
    end
    
    always @(state or start or leftBtn or rightBtn or upBtn or downBtn) begin
        
        casez ({state, start, {leftBtn, downBtn, upBtn, rightBtn}}) 
            {3'bz, 1'b0, 4'bz}: next_state = IDLE;
            {IDLE, 1'b1, 4'bz}: next_state = MENU;
            {MENU, 1'b1, 4'b1000}: begin next_state = RANKVIEW; end
            {MENU, 1'b1, 4'b0001}: begin next_state = GAMESTART; end
            {GAMESTART, 1'b1, 4'bz}: begin next_state = GAMEMAKE; end
            {GAMEMAKE, 1'b1, 4'bz}: begin
                // check finish
                if (finishCheck > 0) next_state = GAMEEND;
                else if ((leftBtn|downBtn|upBtn|rightBtn) && successed==1) begin next_state = GAMEMOVE; end
                else next_state = GAMEMAKE;
            end
            {GAMEMOVE, 1'b1, 4'bz}: begin next_state = GAMEMERGE; end
            {GAMEMERGE, 1'b1, 4'bz}: begin next_state = GAMEMAKE; end
            {GAMEEND, 1'b1, 4'b0001}: begin next_state = MENU; end
            {GAMEEND, 1'b1, 4'b1000}: begin next_state = RANKREGI; end
            {RANKREGI, 1'b1, 4'bz}:  begin next_state = RANKVIEW; end
            {RANKVIEW, 1'b1, 4'b0001}: begin next_state = MENU; end
            {RANKVIEW, 1'b1, 4'b0100}: begin  next_state = RANKVIEW; end
            {RANKVIEW, 1'b1, 4'b0010}: begin  next_state = RANKVIEW; end
            default: begin end
        endcase
    end
    
endmodule
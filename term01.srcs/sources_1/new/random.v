`timescale 1ns / 1ps

module random(
    input clk,
    input rstn,
    output [15:0] rnd
    );

reg [15:0] r_reg;
wire [15:0] r_next;
wire feedback;

assign feedback = r_reg[15] ^ r_reg[14] ^ r_reg[12] ^ r_reg[3];
assign r_next = {feedback, r_reg[15:1]};
assign rnd = r_reg;

always @(posedge clk or negedge rstn) begin
    if (~rstn) begin
        r_reg <= 16'hffff;
    end
    else begin
        r_reg <= r_next;
    end
end

endmodule